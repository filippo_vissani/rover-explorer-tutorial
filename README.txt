Tutorial per principianti su Hololens 2.
Istruzioni:
https://docs.microsoft.com/it-it/windows/mixed-reality/develop/unity/tutorials/mr-learning-base-01

Funzionalità:
- Nel progetto sono presenti un Rover e i suoi componenti disposti su un piano.
- è possibile interagire con il Rover usando le mani o con TapToPlace (al "click" con le dita il Rover si aggancia ad Hololens 2, così è possibile spostarlo usando la testa).
- Sul piano sono presenti 3 bottoni che permettono di far "esplodere" il Rover, visualizzare i componenti mancanti e di resettare gli ologrammi.
- Presenza di un menu che segue l'utente, con possibilità di:
    + Attivare un indicatore che punta al Rover.
    + Attivare TapToPlace.
    + Abilitare/disabilitare un box che permette di scalare la grandezza degli ologrammi.
- Presenza di un secondo Rover ancorato in modo persistente alla mesh (in questo modo la sua posizione rimane la stessa in sessioni diverse)
- Eye tracking abilitato (guardando bottoni e componenti del Rover compariranno delle finestre con informazioni specifiche)
- Suono abilitato
- Comandi vocali abilitati
- Percezione ambientale abilitata, ma non visibile all'utente